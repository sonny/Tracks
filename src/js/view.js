/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported PlaylistView */

const { Gdk, Gio, GLib, GObject, Gtk } = imports.gi;
const Tweener = imports.tweener.tweener;

const TRANSITION_DURATION = 250; // in ms, for cover revealer and and scroll adjustment.

const TrackWidget = GObject.registerClass({
    GTypeName: `${pkg.shortName}TrackWidget`,
    Properties: {
        'air-title': GObject.ParamSpec.string(
            'air-title', "Air title", "A dynamic title tag from an internet radio",
            GObject.ParamFlags.WRITABLE, ''
        ),
        'cover': GObject.ParamSpec.object(
            'cover', "Cover", "The cover paintable",
            GObject.ParamFlags.WRITABLE, GObject.type_from_name(`${pkg.shortName}CoverPaintable`)
        ),
        'expanded-perm': GObject.ParamSpec.boolean(
            'expanded-perm', "Expanded permanently", "Whether the controls are permanently shown",
            GObject.ParamFlags.READWRITE, false
        ),
        'expanded-temp': GObject.ParamSpec.boolean(
            'expanded-temp', "Expanded temporary", "Whether the controls are temporary shown",
            GObject.ParamFlags.READWRITE, false
        ),
        'position': GObject.ParamSpec.uint(
            'position', "Position", "The position of this in the list",
            GObject.ParamFlags.READWRITE, 0, GLib.MAXUINT32, GLib.MAXUINT32
        ),
        'spectrum': GObject.ParamSpec.object(
            'spectrum', "Spectrum", "The spectrum paintable",
            GObject.ParamFlags.WRITABLE, GObject.type_from_name(`${pkg.shortName}SpectrumPaintable`)
        ),
        'track': GObject.ParamSpec.object(
            'track', "Track", "The track this represents",
            GObject.ParamFlags.WRITABLE, GObject.type_from_name(`${pkg.shortName}Track`)
        ),
    },
}, class extends Gtk.Box {
    _init(params) {
        super._init(params);

        this.append(new Gtk.Revealer({
            transitionType: Gtk.RevealerTransitionType.SWING_RIGHT, transitionDuration: TRANSITION_DURATION,
            cssClasses: ['cover'],
        }));

        let grid = new Gtk.Grid({ valign: Gtk.Align.CENTER });

        grid.attach(new Gtk.Label({
            hexpand: true, xalign: 0, ellipsize: 3,
            cssClasses: ['title'],
            ariaRoledescription: _("Title"),
        }), 0, 0, 1, 1);

        grid.attach(new Gtk.Label({
            hexpand: true, xalign: 0, ellipsize: 3,
            cssClasses: ['details'],
            ariaRoledescription: _("%s / %s").format(_("Artists"), _("Album")),
        }), 0, 1, 1, 1);

        grid.attach(new Gtk.Picture({
            halign: Gtk.Align.END, visible: false,
            cssName: 'indicator',
            ariaRoledescription: _("Playing indicator"),
        }), 1, 0, 1, 1);

        grid.attach(new Gtk.Label({
            halign: Gtk.Align.END,
            cssClasses: ['duration'],
            ariaRoledescription: _("Duration"),
        }), 1, 1, 1, 1);

        new Gtk.GesturePan({ widget: grid }).connect('pan', (gesture, direction, offset) => {
            if (offset < 10)
                return;

            this.expandedTemp = direction == Gtk.PanDirection.LEFT;
            gesture.set_state(Gtk.EventSequenceState.CLAIMED);
        });

        this.append(grid);

        this.append(new Gtk.Revealer({
            transitionType: Gtk.RevealerTransitionType.SLIDE_LEFT, transitionDuration: TRANSITION_DURATION,
            revealChild: false, cssClasses: ['remove'],
        }));

        this.ariaLabelledby = [this._titleLabel];
        this.ariaDetails = [this._detailsLabel, this._durationLabel];
        this.ariaDescribedby = [this._playIndicator];
    }

    get _coverRevealer() {
        return this.get_first_child();
    }

    get _titleLabel() {
        return this.get_first_child().get_next_sibling().get_child_at(0, 0);
    }

    get _detailsLabel() {
        return this.get_first_child().get_next_sibling().get_child_at(0, 1);
    }

    get _playIndicator() {
        return this.get_first_child().get_next_sibling().get_child_at(1, 0);
    }

    get _durationLabel() {
        return this.get_first_child().get_next_sibling().get_child_at(1, 1);
    }

    get _controlRevealer() {
        return this.get_last_child();
    }

    _updateExpanded() {
        let expanded = this.expandedPerm || this.expandedTemp;

        if (expanded && !this._controlRevealer.child) {
            this._controlRevealer.child = new Gtk.Box({
                orientation: Gtk.Orientation.VERTICAL,
                vexpand: true,
                canFocus: false,
            }).with(
                new Gtk.Button({
                    vexpand: true, valign: Gtk.Align.CENTER,
                    actionName: 'list.remove-item',
                    actionTarget: GLib.Variant.new_uint32(this.position),
                    iconName: 'list-remove-symbolic',
                    tooltipText: _("Remove track"),
                    cssName: 'flat',
                    css_classes: ['warning'],
                }),
                new Gtk.Button({
                    vexpand: true, valign: Gtk.Align.CENTER,
                    actionName: 'list.info-item',
                    actionTarget: GLib.Variant.new_uint32(this.position),
                    iconName: 'dialog-information-symbolic',
                    tooltipText: _("Track properties"),
                    cssName: 'flat',
                })
            );
        }

        this._controlRevealer.revealChild = expanded;
    }

    set airTitle(airTitle) {
        if (airTitle)
            this._detailsLabel.label = airTitle;
    }

    set cover(cover) {
        if (!cover) {
            this._coverRevealer.child?.set_from_paintable(null);
            this._coverRevealer.revealChild = false;
            return;
        }

        if (!this._coverRevealer.child)
            this._coverRevealer.child = new Gtk.Image();

        this._coverRevealer.child.paintable = cover.texture;

        if (this._coverRevealer.child.get_mapped()) {
            this._coverRevealer.child.pixelSize = this._coverRevealer.get_height() - 2;
        } else {
            let handler = this._coverRevealer.child.connect('map', () => {
                this._coverRevealer.child.pixelSize = this._coverRevealer.get_height() - 2;
                this._coverRevealer.child.disconnect(handler);
            });
        }

        this._coverRevealer.revealChild = true;
    }

    get expandedPerm() {
        return this._expandedPerm || false;
    }

    set expandedPerm(expandedPerm) {
        if (this.expandedPerm == expandedPerm)
            return;

        this._expandedPerm = expandedPerm;
        this.notify('expanded-perm');

        this._updateExpanded();
    }

    get expandedTemp() {
        return this._expandedTemp || false;
    }

    set expandedTemp(expandedTemp) {
        if (this.expandedTemp == expandedTemp)
            return;

        this._expandedTemp = expandedTemp;
        this.notify('expanded-temp');

        this._updateExpanded();
    }

    get position() {
        return this._position ?? GLib.MAXUINT32;
    }

    set position(position) {
        if (this._position === position)
            return;

        this._position = position;
        this.notify('position');

        this._controlRevealer.child?.get_first_child().set_action_target_value(GLib.Variant.new_uint32(this.position));
        this._controlRevealer.child?.get_last_child().set_action_target_value(GLib.Variant.new_uint32(this.position));
    }

    set spectrum(spectrum) {
        this._playIndicator.paintable = spectrum || null;
        this._playIndicator.visible = !!spectrum;

        if (spectrum) {
            spectrum.pangoContext = this._playIndicator.get_pango_context();
            spectrum.styleContext = this._playIndicator.get_style_context();
        }
    }

    set track(track) {
        if (track) {
            this._titleLabel.label = track.title || track.fileName;
            this._detailsLabel.label = track.isWebsiteXml ? track.album || _("Unkown") :
                _("%s / %s").format(track.artist || track.albumArtist || _("Unkown"), track.album || track.folderName || _("Unkown"));
            this._durationLabel.label = track.humanDuration;
        }
    }
});

const TrackFactory = GObject.registerClass({
    GTypeName: `${pkg.shortName}TrackFactory`,
    Properties: {
        'display-cover': GObject.ParamSpec.boolean(
            'display-cover', "Display cover", "Whether the cover is displayed in the selected row",
            GObject.ParamFlags.READWRITE, false
        ),
        'display-controls': GObject.ParamSpec.boolean(
            'display-controls', "Display controls", "Whether controls are displayed for all the rows",
            GObject.ParamFlags.READWRITE, false
        ),
        'player': GObject.ParamSpec.object(
            'player', "Player", "The audio player",
            GObject.ParamFlags.READWRITE, GObject.type_from_name(`${pkg.shortName}Player`)
        ),
        'selected-row': GObject.ParamSpec.object(
            'selected-row', "Selected row", "The selected row",
            GObject.ParamFlags.READWRITE, Gtk.ListItem.$gtype
        ),
    },
}, class extends Gtk.SignalListItemFactory {
    _onCoverChanged() {
        if (this.selectedRow && this.selectedRow.item && this.player.cover != this._coverMap.get(this.selectedRow.item)) {
            this._coverMap.set(this.selectedRow.item, this.player.cover);
            this.selectedRow.child.cover = this.displayCover && this.player.cover;
        }
    }

    // For internet radios.
    _onTagsChanged(player_, airTitle) {
        if (this.selectedRow?.item && !this.selectedRow.item.title)
            this.selectedRow.child.airTitle = airTitle;
    }

    vfunc_constructed() {
        super.vfunc_constructed();

        this._coverMap = new WeakMap();
        pkg.settings.bind('display-covers', this, 'display-cover', Gio.SettingsBindFlags.GET | Gio.SettingsBindFlags.INVERT_BOOLEAN);
    }

    on_notify(pspec) {
        switch(pspec.get_name()) {
            case 'display-cover':
                if (this.selectedRow)
                    this.selectedRow.child.cover = this.displayCover && this._coverMap.get(this.selectedRow.item);
                break;

            case 'player':
                if (this.player) {
                    this.player.connect('tags-changed', this._onTagsChanged.bind(this));
                    this.player.connect('notify::cover', this._onCoverChanged.bind(this));
                }
                break;
        }
    }

    on_setup(row) {
        row.child = new TrackWidget();
        this.bind_property('display-controls', row.child, 'expanded-perm', GObject.BindingFlags.SYNC_CREATE);
    }

    on_bind(row) {
        if (row.selected)
            this.selectedRow = row;

        row.child.cover = row.selected && this.displayCover && this._coverMap.get(this.selectedRow.item);
        row.child.position = row.position;
        row.child.spectrum = row.selected && this.player.spectrum;
        row.child.track = row.item;

        if (!row.child.parent.hasControllers) {
            row.child.parent.hasControllers = true;

            new Gtk.GestureClick({ widget: row.child.parent, button: Gdk.BUTTON_SECONDARY }).connect('released', function(controller) {
                controller.widget.get_first_child().expandedTemp = !controller.widget.get_first_child().expandedTemp;
            });

            new Gtk.EventControllerMotion({ widget: row.child.parent }).connect('leave', function(controller) {
                controller.widget.get_first_child().expandedTemp = false;
            });

            // Handle the key events only when the row has a visible focus (outline) so, outside a row browsing sequence,
            // a row cannot be inadvertently removed and the 'i' key can trigger the search entry.
            new Gtk.EventControllerKey({ widget: row.child.parent }).connect('key-pressed', function(controller, keyval) {
                if ((keyval == Gdk.KEY_Delete || keyval == Gdk.KEY_KP_Delete)
                    && controller.widget.get_state_flags() & (Gtk.StateFlags.FOCUS_VISIBLE | Gtk.StateFlags.SELECTED))
                    controller.widget.activate_action('list.remove-item', GLib.Variant.new_uint32(controller.widget.get_first_child().position));
                else if (keyval == Gdk.KEY_i && controller.widget.get_state_flags() & Gtk.StateFlags.FOCUS_VISIBLE)
                    controller.widget.activate_action('list.info-item', GLib.Variant.new_uint32(controller.widget.get_first_child().position));
                else
                    return Gdk.EVENT_PROPAGATE;

                return Gdk.EVENT_STOP;
            });
        }
    }

    on_unbind(row) {
        row.child.cover = null;
        row.child.spectrum = null;
        row.child.track = null;
    }
});

var PlaylistView = GObject.registerClass({
    GTypeName: `${pkg.shortName}PlaylistView`,
    Properties: {
        'display-row-controls': GObject.ParamSpec.boolean(
            'display-row-controls', "Display row controls", "Whether controls are displayed for all the rows",
            GObject.ParamFlags.READWRITE, false
        ),
    },
    Signals: {
        'info-track': { param_types: [GObject.type_from_name(`${pkg.shortName}Track`)] },
    },
}, class extends Gtk.ListView {
    _init(params) {
        super._init(Object.assign(params, {
            cssClasses: ['rich-list'],
            ariaLabel: _("Playlist"),
        }));

        let action, actionGroup = new Gio.SimpleActionGroup();
        this.insert_action_group('list', actionGroup);

        action = new Gio.SimpleAction({ name: 'info-item', parameterType: GLib.VariantType.new('u') });
        action.connect('activate', (action, parameter) => {
            this.emit('info-track', this.model.get_item(parameter.unpack()));
        });
        actionGroup.add_action(action);

        action = new Gio.SimpleAction({ name: 'remove-item', parameterType: GLib.VariantType.new('u') });
        action.connect('activate', (action, parameter) => {
            this.model.removeTrack(this.model.get_item(parameter.unpack()));
        });
        actionGroup.add_action(action);

        action = new Gio.SimpleAction({ name: 'show-selected-item' });
        action.connect('activate', () => {
            this._updateAdjustment(true);
        });
        actionGroup.add_action(action);

        // Let's play with expressions to determine when the 'show-selected-item' action can be activated.
        let modelExpr = Gtk.PropertyExpression.new(this.constructor.$gtype, null, 'model');
        let selectedExpr = Gtk.PropertyExpression.new(Gtk.SingleSelection.$gtype, modelExpr, 'selected');
        let hasSelectedExpr = Gtk.ClosureExpression.new(GObject.TYPE_UINT, (this_, selected) => {
            return selected != GLib.MAXUINT32;
        }, [selectedExpr]);
        hasSelectedExpr.bind(action, 'enabled', this);
    }

    _updateAdjustment(animate) {
        if (this.model.selected == GLib.MAXUINT32)
            return;

        if (this._timeout) {
            GLib.source_remove(this._timeout);
            delete this._timeout;
        }

        Tweener.removeTweens(this.vadjustment);

        if (!animate) {
            this.activate_action('list.scroll-to-item', GLib.Variant.new_uint32(this.model.selected));
            return;
        }

        let height = this.get_first_child().get_allocated_height();
        let margin = height - 1;
        let { length, selected } = this.model;
        let { pageSize, value, upper } = this.vadjustment;
        let y = Math.ceil(upper * selected / length);
        let newValue;

        if (y < value + margin) {
            newValue = y - margin;
        } else if (y + height > value + pageSize - margin) {
            newValue = y + height - pageSize + margin;
        } else {
            return;
        }

        Tweener.addTween(this.vadjustment, {
            value: newValue,
            time: TRANSITION_DURATION / 1000,
            transition: 'easeInOutCubic',
            rounded: true,
            // Less scroll failures but also less smooth.
            skipUpdates: Math.min(Math.ceil(Math.abs(newValue - value) / (10 * pageSize)), 10),
            onComplete: () => {
                // Scroll failure workaround.
                let i = 0;
                this._timeout = GLib.timeout_add(GLib.PRIORITY_LOW, 50, () => {
                    if (i < 10 && this.vadjustment.value != newValue) {
                        this.vadjustment.value = newValue;
                        i++;
                        return GLib.SOURCE_CONTINUE;
                    }

                    delete this._timeout;
                    return GLib.SOURCE_REMOVE;
                });
            },
        });
    }

    _updateFocusChild() {
        if (this.factory.selectedRow)
            this.set_focus_child(this.factory.selectedRow.child.parent);
    }

    on_activate(position) {
        this.model.activeTrack = this.model.get_item(position);
    }

    on_notify(pspec) {
        if (pspec.get_name() != 'model')
            return;

        // Scroll the list view when a new track is activated.
        this.model.connect('notify::active-track', this._updateAdjustment.bind(this, true));
        // Ensure the selected row stays effectively visible when the list view is filtered.
        this.model.connect('notify::length', () => {
            GLib.timeout_add(GLib.PRIORITY_LOW, 10, this._updateAdjustment.bind(this, false));
        });
        this.model.connect('reset', () => {
            this.vadjustment.value = 0;
        });

        this.factory = new TrackFactory({ player: this.model.player });
        this.bind_property('display-row-controls', this.factory, 'display-controls', GObject.BindingFlags.SYNC_CREATE);
        // Ensure the selected row will stay effectively visible when the list view will receive the focus.
        this.factory.connect('notify::selected-row', this._updateFocusChild.bind(this));
    }
});
