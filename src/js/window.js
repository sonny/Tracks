/*
 * Copyright 2020 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2020 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported ApplicationWindow */

const { Gdk, Gio, GLib, GObject, Gtk } = imports.gi;
const { Player } = imports.player;
const { Wrapper: Scanner } = imports.scanner;
const { PlaylistSelection } = imports.selection;
const { PlaylistView } = imports.view;
const { HeaderBar } = imports.headerbar;
const { AboutDialog, FileChooserDialog, PlaylistLoaderDialog, PlaylistSaverDialog, PrefsDialog, PropsDialog, ShortcutsWindow } = imports.dialogs;

const DEFAULT_WINDOW_SIZE = 400;

const CoverPicture = GObject.registerClass({
    GTypeName: `${pkg.shortName}CoverPicture`,
    Properties: {
        'cover': GObject.ParamSpec.object(
            'cover', "Cover", "The cover paintable",
            GObject.ParamFlags.READWRITE, GObject.type_from_name(`${pkg.shortName}CoverPaintable`)
        ),
        // Not currently exposed to the user, but can be adjusted from GTK Inspector for testing purpose.
        'cover-blur-radius': GObject.ParamSpec.double(
            'cover-blur-radius', "Cover blur radius", "The blur radius to render the cover with",
            GObject.ParamFlags.READWRITE, 0, 100, 0
        ),
        'cover-is-dark': GObject.ParamSpec.boolean(
            'cover-is-dark', "Cover is dark", "Whether the cover is dark",
            GObject.ParamFlags.READABLE, false
        ),
        'display-cover': GObject.ParamSpec.boolean(
            'display-cover', "Display cover", "Whether the cover must be displayed",
            GObject.ParamFlags.READWRITE, false
        ),
        'fade-cover': GObject.ParamSpec.boolean(
            'fade-cover', "Fade cover", "Whether the cover must be fade",
            GObject.ParamFlags.READWRITE, false
        ),
    },
}, class extends Gtk.Picture {
    _updateCoverBlurRadius() {
        if (this.cover)
            this.cover.blurRadius = this.coverBlurRadius;
    }

    _updateCoverFade() {
        if (this.cover)
            this.cover.fade = this.fadeCover;
    }

    _updatePaintable() {
        this.set_paintable(this.cover && this.displayCover ? this.cover : null);
    }

    on_notify(pspec) {
        switch (pspec.get_name()) {
            case 'cover':
            case 'display-cover':
                this._updateCoverFade();
                this._updatePaintable();
                break;
            case 'cover-blur-radius':
                this._updateCoverBlurRadius();
                break;
            case 'fade-cover':
                this._updateCoverFade();
        }
    }

    get coverIsDark() {
        return this.cover?.isDark || false;
    }
});

const ScrolledWindow = GObject.registerClass({
    GTypeName: `${pkg.shortName}ScrolledWindow`,
}, class extends Gtk.ScrolledWindow {
    vfunc_size_allocate(width, height, baseline) {
        super.vfunc_size_allocate(width, height, baseline);

        let rect = this.child.get_allocation();
        rect.width = Math.min(width, Math.max(this.parent.get_allocated_height(), DEFAULT_WINDOW_SIZE));
        rect.x = (width - rect.width) / 2;

        this.child.size_allocate(rect, -1);
    }
});

Gtk.Window.set_default_icon_name(pkg.name);
var ApplicationWindow = GObject.registerClass({
    GTypeName: `${pkg.shortName}ApplicationWindow`,
    Properties: {
        'player': GObject.ParamSpec.object(
            'player', "Player", "The audio player",
            GObject.ParamFlags.READWRITE, Player.$gtype
        ),
        'playlists': GObject.ParamSpec.object(
            'playlists', "Playlists", "The playlists",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, GObject.type_from_name(`${pkg.shortName}Playlists`)
        ),
        'scanner': GObject.ParamSpec.jsobject(
            'scanner', "Scanner", "The file scanner",
            GObject.ParamFlags.READWRITE
        ),
        'selection': GObject.ParamSpec.object(
            'selection', "Selection", "The model of the playlist view",
            GObject.ParamFlags.READWRITE, GObject.type_from_name(`${pkg.shortName}PlaylistSelection`)
        ),
    },
}, class extends Gtk.ApplicationWindow {
    _init(params = {}) {
        super._init(Object.assign(params, { name: 'Window' }));
        this.add_css_class(pkg.styleClass);
        this.add_css_class('empty');

        let provider = new Gtk.CssProvider();
        provider.load_from_resource(`${this.application.resource_base_path}/stylesheet.css`);
        Gtk.StyleContext.add_provider_for_display(this.get_display(), provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

        this.scanner = new Scanner(this.get_id());
        this.scanner.connect('error-occurred', this._onErrorOccurred.bind(this));
        this.scanner.connect('files-scanned', this._onFilesScanned.bind(this));
        this.scanner.connect('busy-changed', this._onBusyChanged.bind(this));

        this.player = new Player();
        this.player.connect('error-occurred', this._onErrorOccurred.bind(this));
        pkg.settings.bind('spectrum-indicator', this.player, 'spectrum-active', Gio.SettingsBindFlags.GET);

        this.selection = new PlaylistSelection({ player: this.player, playlist: this.playlists.getEmptyPlaylist() });
        this.selection.connect('notify::has-tracks', selection => this[selection.hasTracks ? 'remove_css_class' : 'add_css_class']('empty'));
        this.selection.connect('error-occurred', this._onErrorOccurred.bind(this));

        if (this.get_id() == 1 && this.application.isPrimaryInstance && pkg.settings.get_boolean('remember-playlist'))
            this.playlists.getDefaultPlaylistAsync().then(playlist => (this.selection.playlist = playlist)).catch(logError);

        this.child = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });

        if (pkg.settings.get_boolean('use-titlebar')) {
            let headerBarContainer = new Gtk.Box().with(new HeaderBar({ player: this.player, is_titlebar: false }));
            this.bind_property('fullscreened', headerBarContainer, 'visible', GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.INVERT_BOOLEAN);
            this.child.append(headerBarContainer);
            let headerBarHeight = headerBarContainer.get_first_child().get_preferred_size()[1].height;
            this.set_default_size(DEFAULT_WINDOW_SIZE, DEFAULT_WINDOW_SIZE + headerBarHeight);
        } else {
            this.set_titlebar(new HeaderBar({ player: this.player }));
            let headerBarHeight = this.get_titlebar().get_preferred_size()[1].height;
            this.set_default_size(DEFAULT_WINDOW_SIZE - 5 - headerBarHeight, DEFAULT_WINDOW_SIZE - 5);
        }

        let overlay, dragImage, fullscreenBarContainer, searchBar, listView;
        this.child.append(overlay = new Gtk.Overlay({ vexpand: true }));

        overlay.child = new CoverPicture({ name: 'CoverPicture', ariaLabel: _('Cover') });
        overlay.child.connect('notify::paintable', this._onCoverUpdated.bind(this));
        pkg.settings.bind('display-covers', overlay.child, 'display-cover', Gio.SettingsBindFlags.GET);
        pkg.settings.bind('fade-covers', overlay.child, 'fade-cover', Gio.SettingsBindFlags.GET);
        this.player.bind_property('cover', overlay.child, 'cover', GObject.BindingFlags.SYNC_CREATE);

        overlay.add_overlay(new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL }).with(
            fullscreenBarContainer = new Gtk.Box({
                visible: false,
            }),
            searchBar = new Gtk.SearchBar({
                showCloseButton: true,
                keyCaptureWidget: this,
                child: new Gtk.SearchEntry({ tooltipText: _("Filter the playlist") }),
            }),
            new ScrolledWindow({
                vexpand: true,
                child: listView = new PlaylistView({
                    halign: Gtk.Align.FILL, valign: Gtk.Align.FILL,
                    model: this.selection,
                }),
            })
        ));
        this.bind_property('fullscreened', fullscreenBarContainer, 'visible', GObject.BindingFlags.DEFAULT);
        fullscreenBarContainer.connect('notify::visible', this._onFullscreenBarContainerVisiblityChanged.bind(this));
        searchBar.ariaControls = [listView];
        searchBar.child.connect('search-changed', entry => this.selection.filter(entry.text));
        listView.connect('info-track', (listView, track) => this._showDialog(PropsDialog, track));

        overlay.add_overlay(dragImage = new Gtk.Image({
            name: 'DragImage', visible: false,
            ariaDescription: _("Add dragged file to the playlist"),
        }));

        let shortcutController = new Gtk.ShortcutController({ propagationPhase: Gtk.PropagationPhase.CAPTURE, widget: this });
        this._addSimpleActions([
            // [name, callback, 'enabled' binding source, binding source property, binding flags]
            ['quit', () => this.close()],
            ['about', () => this._showDialog(AboutDialog)],
            ['prefs', () => this._showDialog(PrefsDialog)],
            ['props', () => this._showDialog(PropsDialog, this.selection.activeTrack), this.player, 'active', GObject.BindingFlags.SYNC_CREATE],
            ['shortcuts', () => this._showDialog(ShortcutsWindow)],
            ['fullscreen', this._toggleFullscreen.bind(this)],
            ['open-file-chooser', () => this._showDialog(FileChooserDialog)],
            ['open-playlist-saver', () => this._showDialog(PlaylistSaverDialog), this.selection, 'has-tracks', GObject.BindingFlags.SYNC_CREATE],
            ['open-playlist-loader', () => this._showDialog(PlaylistLoaderDialog)],
            ['view-show-selected-row', () => listView.activate_action('list.show-selected-item', null)],
            ['empty-playlist', () => this.selection.empty(), this.selection, 'has-tracks', GObject.BindingFlags.SYNC_CREATE],
            ['skip-backward', () => this.selection.skipBackward(), this.selection, 'back-skippable', GObject.BindingFlags.SYNC_CREATE],
            ['skip-forward', () => this.selection.skipForward(), this.selection, 'for-skippable', GObject.BindingFlags.SYNC_CREATE],
            ['go-backward', () => this.player.goBackward(), this.player, 'active', GObject.BindingFlags.SYNC_CREATE],
            ['go-forward', () => this.player.goForward(), this.player, 'active', GObject.BindingFlags.SYNC_CREATE],
        ], shortcutController);
        this._addPropertyActions([
            // [name, object, property]
            ['enable-search', searchBar, 'search-mode-enabled'],
            ['pause', this.player, 'paused'],
            ['playlist-shuffle', this.selection, 'shuffle'],
            ['playlist-sort-ascending', this.selection, 'sort-ascending'],
            ['playlist-sort-descending', this.selection, 'sort-descending'],
            ['view-display-row-controls', listView, 'display-row-controls'],
        ], shortcutController);
        this.lookup_action('open-playlist-loader').set_enabled(this.playlists.get_n_items() > 0);
        this.playlists.connect('items-changed', () => this.lookup_action('open-playlist-loader').set_enabled(this.playlists.get_n_items() > 0));

        let keyController = new Gtk.EventControllerKey({ propagationPhase: Gtk.PropagationPhase.CAPTURE, widget: this });
        keyController.connect('key-pressed', this._onKeyPressed.bind(this, searchBar));

        let dropTarget = new Gtk.DropTarget({ actions: Gdk.DragAction.COPY, widget: this });
        dropTarget.set_gtypes([GObject.TYPE_STRING, Gio.File.$gtype]);
        dropTarget.connect('enter', this._onDragEntered.bind(this, dragImage));
        dropTarget.connect('leave', this._onDragLeft.bind(this, dragImage));
        dropTarget.connect('drop', this._onDragDropped.bind(this, dragImage));
    }

    _onDragEntered(dragImage, dropTarget) {
        dragImage.set_visible(true);
        return dropTarget.actions;
    }

    _onDragLeft(dragImage) {
        dragImage.set_visible(false);
    }

    _onDragDropped(dragImage, dropTarget, data) {
        this._onDragLeft(dragImage);

        if (typeof data == 'string') {
            let uris = data.split('\n').map(uri => uri.trim()).filter(uri => uri);
            this.scanner.scanFiles(uris.map(uri => Gio.File.parse_name(uri)), false);
        } else if (data instanceof Gio.File) {
            this.scanner.scanFiles([data], false);
        } else {
            return false;
        }

        return true;
    }

    _onKeyPressed(searchBar, keyController, keyval) {
        let entryDelegate = searchBar.child.get_delegate();

        if (this.fullscreened && !entryDelegate.hasFocus && keyval == Gdk.KEY_Escape) {
            this._toggleFullscreen();
            return Gdk.EVENT_STOP;
        }

        if (entryDelegate.hasFocus && keyval == Gdk.KEY_Down) {
            let listView = searchBar.get_next_sibling().child;
            listView.grab_focus();
            return Gdk.EVENT_STOP;
        }

        // Bypass the 'win.pause' action that handle the 'space' key.
        if (entryDelegate.hasFocus && keyval == Gdk.KEY_space)
            return keyController.forward(entryDelegate);

        return Gdk.EVENT_PROPAGATE;
    }

    _addAccelerators(name, controller) {
        if (!pkg.shortcutSettings.settings_schema.has_key(name))
            return;

        let shortcut = new Gtk.Shortcut({ action: new Gtk.NamedAction({ actionName: `win.${name}` }) });
        pkg.shortcutSettings.bindWithMapping(name, shortcut, 'trigger', Gio.SettingsBindFlags.GET, variant => {
            let accelerators = variant.deep_unpack();
            let trigger = Gtk.ShortcutTrigger.parse_string(accelerators.join('|'));
            return [!!trigger, trigger];
        });

        controller.add_shortcut(shortcut);
    }

    _addPropertyActions(actions, shortcutController) {
        actions.forEach(([name, object, propertyName]) => {
            let action = new Gio.PropertyAction({ name, object, propertyName });
            this.add_action(action);
            this._addAccelerators(name, shortcutController);
        });
    }

    _addSimpleActions(actions, shortcutController) {
        actions.forEach(([name, callback, bindingSource, bindingSourceProperty, bindingFlags]) => {
            let action = new Gio.SimpleAction({ name });
            action.connect('activate', callback);
            this.add_action(action);
            this._addAccelerators(name, shortcutController);
            if (bindingSource)
                bindingSource.bind_property(bindingSourceProperty, action, 'enabled', bindingFlags);
        });
    }

    vfunc_close_request() {
        this.player.stop();
        if (this.application.get_windows().length == 1)
            this.playlists.disable();
        if (this.get_id() == 1 && this.application.isPrimaryInstance && pkg.settings.get_boolean('remember-playlist'))
            this.selection.playlist.saveAsDefaultAsync().catch(logError);
        this.selection.playlist = null;

        this.destroy();
    }

    _onErrorOccurred(emitter_, message) {
        let notification = new Gio.Notification();
        notification.set_title(pkg.shortName);
        notification.set_body(message);
        // Set message content as notificaton id
        // so 2 equal messages use the same notification.
        this.application.send_notification(message, notification);
    }

    _onFilesScanned(scanner_, list, index, play) {
        this.selection.add(list, index, play);
    }

    _onBusyChanged(emitter_, busy) {
        this.get_root().set_cursor(busy ? Gdk.Cursor.new_from_name("wait", null) : null);
    }

    _onFullscreenBarContainerVisiblityChanged(fullscreenBarContainer) {
        if (fullscreenBarContainer.visible) {
            let fullscreenBar = new HeaderBar({ player: this.player, isTitlebar: false, isFullscreenBar: true });
            fullscreenBar.syncWithTitlebar(this.get_titlebar() || this.child.get_first_child().get_first_child());
            fullscreenBarContainer.append(fullscreenBar);
        } else {
            fullscreenBarContainer.remove(fullscreenBarContainer.get_first_child());
        }
    }

    _toggleFullscreen() {
        this.fullscreened = !this.fullscreened;
    }

    // Css transitions make in-list moving painful.
    // Use transition only for the style changes related to the cover.
    _updateCssClass(cssClass, add) {
        if (this.has_css_class(cssClass) == add)
            return;

        this.add_css_class('transition');
        this[add ? 'add_css_class' : 'remove_css_class'](cssClass);

        if (this._cssTransitionTimeout)
            GLib.source_remove(this._cssTransitionTimeout);

        this._cssTransitionTimeout = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 400, () => {
            this.remove_css_class('transition');
            this._cssTransitionTimeout = null;
            return GLib.SOURCE_REMOVE;
        });
    }

    _onCoverUpdated(coverPicture) {
        this._updateCssClass('cover', !!coverPicture.paintable);

        if (coverPicture.paintable) {
            let color = this.get_style_context().get_color();
            let themeIsDark = color.red + color.green + color.blue / 3 > 0.5;
            this._updateCssClass('inverted', coverPicture.coverIsDark != themeIsDark);
        }
    }

    _showDialog(constructor, track = undefined) {
        let dialog = Gtk.Window.list_toplevels().find(window => {
            return window.get_transient_for() == this
                && window instanceof constructor
                && (track ? window.track == track : true);
        });

        if (!dialog) {
            dialog = track ? new constructor({ transientFor: this, modal: true, track }) :
                new constructor({ transientFor: this, modal: true });

            // Becomes non-modal once positioned upon the parent window.
            dialog.connect('map', () => {
                GLib.timeout_add(GLib.PRIORITY_LOW, 100, () => dialog.set_modal(false));
            });
        }

        dialog.present();
    }

    notifyError(message) {
        this._onErrorOccurred(null, message);
    }
});
